# frozen_string_literal: true

# Redmine - project management software
# Copyright (C) 2006-2022  Jean-Philippe Lang
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class WeeklyUpdateController < ApplicationController
  default_search_scope :weekly_update
  model_object WeeklyUpdate
  before_action :find_model_object, :except => [:new, :create, :index]
  before_action :find_project_from_association, :except => [:new, :create, :index]
  before_action :find_project_by_project_id, :only => :create
  before_action :authorize, :except => [:index, :new]
  before_action :find_optional_project, :only => [:index, :new]
  accept_atom_auth :index
  accept_api_auth :index, :show, :create, :update, :destroy

  helper :watchers
  helper :attachments

  def index
    case params[:format]
    when 'xml', 'json'
      @offset, @limit = api_offset_and_limit
    else
      @limit = 10
    end

    scope = WeeklyUpdate.visible

    @project = Project.find(params[:project_id])
    @current_user = User.current
    @logs = TodoProjectLogs.find_by_sql("select * from todo_project_logs where todo_project_id =#{@project.id} and tab_type='weekly-update' and create_by='#{@current_user.id}'")
    if @logs.present?
      @logs.each do |item|
        item.delete
      end
    end

    @news_count = scope.count
    @news_pages = Paginator.new @news_count, @limit, params['page']
    @offset ||= @news_pages.offset
    @newss = scope.includes([:author, :project]).
      order("#{WeeklyUpdate.table_name}.created_on DESC").
      limit(@limit).
      offset(@offset).
      to_a
    respond_to do |format|
      format.html do
        @news = WeeklyUpdate.new # for adding news inline
        render :layout => false if request.xhr?
      end
      format.api
      format.atom do
        render_feed(
          @newss,
          :title =>
            (@project ? @project.name : Setting.app_title) +
              ": #{l(:label_weekly_update_plural)}"
        )
      end
    end
  end

  def show
    @comments = @news.comments.to_a
    @comments.reverse! if User.current.wants_comments_in_reverse_order?
  end

  def new
    raise ::Unauthorized unless User.current.allowed_to?(:manage_weekly_update, @project, :global => true)

    @news = WeeklyUpdate.new(:project => @project, :author => User.current)
  end

  def create
    @news = WeeklyUpdate.new(:project => @project, :author => User.current)
    @news.safe_attributes = params[:weekly_update]
    @news.save_attachments(params[:attachments] || (params[:weekly_update] && params[:weekly_update][:uploads]))
    # save logs
    if @project.users.present?
      @users = @project.users.to_a
      @users.each do |item|
        @log = TodoProjectLogs.new
        @log.tab_type = 'weekly-update'
        @log.create_by = item[:id]
        @log.status = 1
        @log.todo_project_id = @project.id
        @log.save
      end
    end
    if @news.save
      respond_to do |format|
        format.html do
          render_attachment_warning_if_needed(@news)
          flash[:notice] = l(:notice_successful_create)
          redirect_to params[:cross_project] ? weekly_update_index_path : project_weekly_update_index_path(@project)
        end
        format.api { render_api_ok }
      end
    else
      respond_to do |format|
        format.html { render :action => 'new' }
        format.api { render_validation_errors(@news) }
      end
    end
  end

  def edit
  end

  def update
    @news.safe_attributes = params[:weekly_update]
    @news.save_attachments(params[:attachments] || (params[:weekly_update] && params[:weekly_update][:uploads]))
    # save logs
    if @project.users.present?
      @users = @project.users.to_a
      @users.each do |item|
        @log = TodoProjectLogs.new
        @log.tab_type = 'weekly-update'
        @log.create_by = item[:id]
        @log.status = 1
        @log.todo_project_id = @project.id
        @log.save
      end
    end

    if @news.save
      respond_to do |format|
        format.html do
          render_attachment_warning_if_needed(@news)
          flash[:notice] = l(:notice_successful_update)
          redirect_to weekly_update_path(@news)
        end
        format.api { render_api_ok }
      end
    else
      respond_to do |format|
        format.html { render :action => 'edit' }
        format.api { render_validation_errors(@news) }
      end
    end
  end

  def destroy
    @news.destroy
    respond_to do |format|
      format.html do
        flash[:notice] = l(:notice_successful_delete)
        redirect_to project_weekly_update_index_path(@project)
      end
      format.api { render_api_ok }
    end
  end
end
